import os

IMAGE_FILE = None
# string pointing to file 
COORDINATES = None
# array of two tuples, one tuple for each tap
USAGE_DATA = None
# array of arrays: one array for each tap
SCALING = 0.1
# scale factor

no_taps = len(COORDINATES)

def sanity_checks():
    '''Check that data is well formated and all files exist'''
    global IMAGE_FILE
    global COORDINATES
    global USAGE_DATA
    # check image file exists
    if not os.path.isfile(IMAGE_FILE):
        print('Image file {} does not exist'.format(IMAGE_FILE))
        exit(1)
    no_taps = len(COORDINATES)
    if not len(USAGE_DATA) == no_taps:
        print('Input data mismatch: number of coordinates {}, number of data rows {}'.format(no_taps, len(USAGE_DATA)))
        exit(1)
    if no_taps == 0:
        print('0 taps!!')
        exit(1)
    no_points_per_tap = len(USAGE_DATA[0])
	for row in USAGE_DATA:
    	if not len(row) == no_points_per_tap:
        	print('Input data mismatch, not all rows have same number of columns')
        	exit(1)

